package tp3;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Compter {
	public static void main(String args[]) throws Exception {

//		// enregistrement du driver
//		//Class.forName("org.postgresql.Driver");
//		Class.forName("org.h2.Driver");
//
//		// connexion à la base
//		//String url = "jdbc:postgresql://psqlserv/but2";
//		String url = "jdbc:h2:tcp://localhost/~/test";
//		String nom = "nathan";
//		String mdp = "mdp";
		
		DS cnx = new DS("config.prop");
		Connection con = cnx.getConnexion();
		
		try(con){
			// exécution de la requete
			Statement stmt = con.createStatement();

			String query = "select COUNT(*) from CLIENTS";
			ResultSet rs = stmt.executeQuery(query);
			rs.next();
			System.out.println("Liste des clients:" + rs.getInt(1));


		}
		catch (Exception e){
			e.printStackTrace();
		}

		System.out.println("All is ok !");
	}
}