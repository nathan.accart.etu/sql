package tp3;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class Insert {
	public static void main(String args[]) throws Exception {

		// enregistrement du driver
		//Class.forName("org.postgresql.Driver");
		DS cnx = new DS("config.prop");
		Connection con = cnx.getConnexion();
		
		try(con){
			// exécution de la requete
			Statement stmt = con.createStatement();
			for(int i = 0; i <= 1000; i++) {
				stmt.executeUpdate("insert into CLIENTS " +
						"values('nom"+i+"', 'paul',10)" );
			}

		}
		catch (Exception e){
			e.printStackTrace();
		}

		System.out.println("All is ok !");
	}
}
