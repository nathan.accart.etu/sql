package tp3;
import java.sql.*;

public class Create {
	public static void main(String args[]) throws Exception {
		
//		// enregistrement du driver
//			Class.forName(DS.getProperty("driver"));
		DS cnx = new DS("config.prop");
		Connection con = cnx.getConnexion();
		
		try(con){
			// exécution de la requete
			Statement stmt = con.createStatement();
			stmt.executeUpdate("DROP TABLE IF EXISTS CLIENTS");
			stmt.executeUpdate("create table CLIENTS " +
					"(NOM varchar(10), PRENOM varchar(10), AGE int)" );
		}
		catch (Exception e){
			e.printStackTrace();
		}
		
		System.out.println("All is ok !");
	}
}
