package tp3;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Select {
	public static void main(String args[]) throws Exception {

		// enregistrement du driver
		//Class.forName("org.postgresql.Driver");
		DS cnx = new DS("config.prop");
		Connection con = cnx.getConnexion();
		
		try(con){
			// exécution de la requete
			Statement stmt = con.createStatement();

			String query = "select NOM,PRENOM,AGE from CLIENTS";
			ResultSet rs = stmt.executeQuery(query);
			System.out.println("Liste des clients:");
			while (rs.next())
			{
				String n = rs.getString("nom"); // col 1
				String p = rs.getString("prenom"); // col 2
				int a = rs.getInt("age"); // col 3
				System.out.println(n + " " + p + " " + a);
			}

		}
		catch (Exception e){
			e.printStackTrace();
		}

		System.out.println("All is ok !");
	}
}
