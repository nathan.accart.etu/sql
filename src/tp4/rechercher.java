package tp4;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.util.Scanner;

import tp3.DS;

public class rechercher {
		public static void main(String args[]) throws Exception {

			// enregistrement du driver
			//Class.forName("org.postgresql.Driver");
			DS cnx = new DS("config.prop");
			Connection con = cnx.getConnexion();
			

			try(con){
				// exécution de la requete
				Statement stmt = con.createStatement();
				String query = "SELECT * FROM " + args[0];
				ResultSet rs = stmt.executeQuery(query);
				
				ResultSetMetaData rsmd = rs.getMetaData();
			    int numberOfColumns = rsmd.getColumnCount();
			    System.out.println("Nombre de colonne = " + numberOfColumns + "\n");

			    String colName = "";
			    for(int i = 1; i <= numberOfColumns; i++) {
			    	colName = colName + rsmd.getColumnName(i);
			    		
			    	if(i != numberOfColumns) {
			    		colName = colName +  " / ";
			    	}
			    }
			    System.out.println(colName);
			    String info = "";
			    while (rs.next()) {
			    	for(int i = 1; i <= numberOfColumns; i++) {
			    		info = info + rs.getString(rsmd.getColumnName(i));
			    		
				    	if(i != numberOfColumns) {
				    		info = info +  " / ";
				    	}
			    	}
			    	System.out.println(info);
			    	info = "";
			    }

			}
			catch (Exception e){
				e.printStackTrace();
			}

			System.out.println("All is ok !");
		}
}
