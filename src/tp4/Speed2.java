package tp4;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;

public class Speed2 {
	public static void main(String args[]) throws Exception {

		// enregistrement du driver
		//Class.forName("org.postgresql.Driver");
		DS cnx = new DS("config.prop");
		Connection con = cnx.getConnexion();
		
		long start = System.currentTimeMillis();
		
		try(con){
			// exécution de la requete
			String query = "insert into clients values (?,?,?)";
			PreparedStatement ps = con.prepareStatement(query);
			for (int i = 0; i < 1000; i++) {
				ps.setString(1, "Durand" + i);
				ps.setString(2, "paul" + i);
				ps.setInt(3, i);
				ps.executeUpdate();
			}
			System.out.println("Temps mis : " + (System.currentTimeMillis() - start));

		}
		catch (Exception e){
			e.printStackTrace();
		}

		System.out.println("All is ok !");
	}
}
