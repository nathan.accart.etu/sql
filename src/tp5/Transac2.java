package tp5;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Transac2 {
	public static void main(String args[]) throws Exception {

		// enregistrement du driver
		//Class.forName("org.postgresql.Driver");
		DS cnx = new DS("config.prop");
		Connection con = cnx.getConnexion();
		con.setAutoCommit(false);
		Statement stmt = null;
		
		try{
			// exécution de la requete
			stmt = con.createStatement();
			stmt.executeUpdate("begin;");
			
			//stmt.executeUpdate("CREATE TABLE produit" + "(prod text, qute int);" );
			//stmt.executeUpdate("INSERT into produit values ('chaise', 3);");
			//stmt.executeUpdate("UPDATE compte SET solde = 0; UPDATE produit SET qute = 2 where prod = 'chaise';");
			
			String pers = args[0];
			String prod = args[1];
			
			String r1 = "select qute from produit where prod='" + prod + "'";
			ResultSet rs = stmt.executeQuery(r1);
			rs.next();
			if (rs.getInt("qute") > 0) {
				String r3 = "update compte set solde=solde+100 where pers='" + pers + "'";
				stmt.executeUpdate(r3);
				
				Thread.sleep(20000);
				
				String r2 = "update produit set qute=qute-1 where prod='" + prod + "'";
				stmt.executeUpdate(r2);
				System.out.println("Achat effectué");
			} else {
				System.out.println("Achat impossible");
			}
			stmt.executeUpdate("commit;");

		}
		catch (Exception e){
			System.out.println(e.getMessage());
			stmt.executeUpdate("ROLLBACK;");
			
		}
		finally{
			con.close();
		}

		System.out.println("All is ok !");
	}
}
