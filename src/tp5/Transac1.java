package tp5;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class Transac1 {
	public static void main(String args[]) throws Exception {

		// enregistrement du driver
		//Class.forName("org.postgresql.Driver");
		DS cnx = new DS("config.prop");
		Connection con = cnx.getConnexion();
		con.setAutoCommit(false);
		Statement stmt = null;
		
		try{
			// exécution de la requete
			stmt = con.createStatement();
			stmt.executeUpdate("begin;");
			stmt.executeUpdate("TRUNCATE TABLE CLIENT");
			
			for(int i = 1; i <= 1000; i++) {
				stmt.executeUpdate("insert into CLIENT " +
						"values("+i+",'Jean', 'paul',20)" );
			}
			
			for(int i = 3000; i <= 4000; i++) {
				stmt.executeUpdate("insert into CLIENT " +
						"values("+i+",'Jean', 'paul',30)" );
			}
			stmt.executeUpdate("commit;");

		}
		catch (Exception e){
			System.out.println(e.getMessage());
			stmt.executeUpdate("ROLLBACK;");
			
		}
		finally{
			con.close();
		}

		System.out.println("All is ok !");
	}
}
